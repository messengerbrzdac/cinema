package org.example;

import java.util.ArrayList;
import java.util.List;

public class CinemaHalls {

    static private List<Hall> halls = new ArrayList<>();

    public CinemaHalls(List<Hall> halls){
        for(Hall h: halls){
            this.halls.add(h);
        }
    }

    public static List<Hall> getHalls() {
        return halls;
    }
    public static Hall getHall(Integer id){
        for(Hall h: halls) {
            if(h.getId() == id){
                return new Hall(id);
            } else {
                return null;
            }
        }
        return null;
    }
}
