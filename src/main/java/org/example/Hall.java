package org.example;

import java.util.ArrayList;
import java.util.List;

public class Hall {
    private Integer id;
    private List<Integer> checkedSeats = new ArrayList<Integer>();

    private Movie currentMovie;

    public Hall() {

    }

    public Hall(Integer id) {
        this.id = id;
    }
    public Hall(Integer id, Movie movie) {
        this.id = id;
        this.currentMovie = movie;
    }


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getCheckedSeats() {
        return this.checkedSeats;
    }

    public void setCheckedSeats(List<Integer> checkedSeats) {
        this.checkedSeats = checkedSeats;
    }

    public Movie getCurrentMovie(){
        return this.currentMovie;
    }

    public void setCurrentMovie(Movie movie){
        this.currentMovie = movie;
    }
}
