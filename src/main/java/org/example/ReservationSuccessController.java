package org.example;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;

public class ReservationSuccessController {

    @FXML
    private HBox myHBox;

    Stage stage = null;

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }

    @FXML
    private void exit() {
        Platform.exit();
    }

    @FXML
    private void minimize(ActionEvent event) throws IOException {
        stage = (Stage) myHBox.getScene().getWindow();
        stage.setIconified(true);
    }
}
