package org.example;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


public class PrimaryController implements Initializable {

    @FXML
    private HBox myHBox;

    Stage stage = null;

    private List<Movie> movies = new ArrayList<>();
    //private CsvReader csvReader = new CsvReader();
    //private List<Movie> moviesList = csvReader.getMoviesList();
    private List<Movie> moviesList = CsvReader.getMoviesList();
    private List<Hall> halls = new ArrayList<>();
    public static CinemaHalls cinemaHalls;

    private List<Movie> getData(){
        List<Movie> movies = new ArrayList<>();
        //CsvReader csvReader = new CsvReader();
        //List<Movie> moviesList = csvReader.getMoviesList();
        List<Movie> moviesList = CsvReader.getMoviesList();

        Movie movie;

        for (Movie m: moviesList){
            movie = new Movie();
            movie.setId(m.getId());
            movie.setTitle(m.getTitle());
            movie.setDescription_tag(m.getDescription_tag());
            movie.setDubbing_or_subtitles(m.getDubbing_or_subtitles());
            movie.setStartingHours(m.getStartingHours());
            movie.setDay(m.getDay());
            movies.add(movie);
        }
        return movies;
    }

    @FXML
    GridPane grid;

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }
    @FXML
    private void exit() {
        Platform.exit();
    }

    @FXML
    private void minimize(ActionEvent event) throws IOException {
        stage = (Stage) myHBox.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void maximize(ActionEvent event) throws IOException {
        stage = (Stage) myHBox.getScene().getWindow();
        if(stage.isMaximized()){
            stage.setMaximized(false);
        }
        else {
            stage.setMaximized(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        movies.addAll(getData());
        Integer temp = 0;
        for(int i = 0; i < movies.size();i++){
            for(int j = 0; j < 3; j++){
                halls.add(new Hall(temp));
                temp++;
            }
        }

        cinemaHalls = new CinemaHalls(halls);

        int column = 0;
        int row = 0;
        try {
            for (int i = 0; i < movies.size(); i++) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("item.fxml"));

                AnchorPane anchorPane = fxmlLoader.load();

                ItemController itemController = fxmlLoader.getController();
                itemController.setData(movies.get(i));

                grid.add(anchorPane, column, row++);
                //GridPane.setMargin(anchorPane, new Insets(10));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
