package org.example;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class SecondaryController implements Initializable {
    @FXML
    private HBox myHBox;

    @FXML
    private ToggleButton button1;
    @FXML
    private ToggleButton button2;
    @FXML
    private ToggleButton button3;
    @FXML
    private ToggleButton button4;
    @FXML
    private ToggleButton button5;
    @FXML
    private ToggleButton button6;
    @FXML
    private ToggleButton button7;
    @FXML
    private ToggleButton button8;
    @FXML
    private ToggleButton button9;
    @FXML
    private ToggleButton button10;
    @FXML
    private ToggleButton button11;
    @FXML
    private ToggleButton button12;
    @FXML
    private ToggleButton button13;
    @FXML
    private ToggleButton button14;
    @FXML
    private ToggleButton button15;
    @FXML
    private ToggleButton button16;
    @FXML
    private ToggleButton button17;
    @FXML
    private ToggleButton button18;
    @FXML
    private ToggleButton button19;
    @FXML
    private ToggleButton button20;
    @FXML
    private ToggleButton button21;
    @FXML
    private ToggleButton button22;
    @FXML
    private ToggleButton button23;
    @FXML
    private ToggleButton button24;
    @FXML
    private ToggleButton button25;
    @FXML
    private ToggleButton button26;
    @FXML
    private ToggleButton button27;
    @FXML
    private ToggleButton button28;
    @FXML
    private ToggleButton button29;
    @FXML
    private ToggleButton button30;
    @FXML
    private ToggleButton button31;
    @FXML
    private ToggleButton button32;
    @FXML
    private ToggleButton button33;
    @FXML
    private ToggleButton button34;
    @FXML
    private ToggleButton button35;
    @FXML
    private ToggleButton button36;
    @FXML
    private ToggleButton button37;
    @FXML
    private ToggleButton button38;
    @FXML
    private ToggleButton button39;
    @FXML
    private ToggleButton button40;
    @FXML
    private ToggleButton button41;
    @FXML
    private ToggleButton button42;
    @FXML
    private ToggleButton button43;
    @FXML
    private ToggleButton button44;
    @FXML
    private ToggleButton button45;
    @FXML
    private ToggleButton button46;
    @FXML
    private ToggleButton button47;
    @FXML
    private ToggleButton button48;
    @FXML
    private ToggleButton button49;
    @FXML
    private ToggleButton button50;
    @FXML
    private ToggleButton button51;
    @FXML
    private ToggleButton button52;
    @FXML
    private ToggleButton button53;
    @FXML
    private ToggleButton button54;

    Stage stage = null;

    private List<ToggleButton> toggleButtons = new ArrayList<ToggleButton>();

    public static List<Integer> checkedSeats = new ArrayList<Integer>();

    public static List<Integer> currentCheckedSeats = new ArrayList<Integer>();

    public static Integer currentHallId;

    public static Movie currentMovie;

    public static Hall currentHall;

    public static String currentHour;

    private List<Hall> halls;

    @FXML
    private void switchToPrimary() throws IOException {
        currentCheckedSeats.clear();
        App.setRoot("primary");
    }
    @FXML
    private void switchToReservation() throws IOException {
        for (ToggleButton tb: toggleButtons){
            if (tb.isSelected()){
                checkedSeats.add(Integer.valueOf(tb.getText()));
                currentCheckedSeats.add(Integer.valueOf(tb.getText()));
            }
        }
        App.setRoot("reservation");
    }

    @FXML
    private void exit() {
        currentCheckedSeats.clear();
        Platform.exit();
    }

    @FXML
    private void minimize(ActionEvent event) throws IOException {
        stage = (Stage) myHBox.getScene().getWindow();
        stage.setIconified(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        halls = CinemaHalls.getHalls();
        currentHall = halls.get(currentHallId);

        if(!currentHall.getCheckedSeats().isEmpty()) {
            for (Integer s : currentHall.getCheckedSeats()) {
                switch (s) {
                    case 1:
                        button1.setDisable(true);
                        break;
                    case 2:
                        button2.setDisable(true);
                        break;
                    case 3:
                        button3.setDisable(true);
                        break;
                    case 4:
                        button4.setDisable(true);
                        break;
                    case 5:
                        button5.setDisable(true);
                        break;
                    case 6:
                        button6.setDisable(true);
                        break;
                    case 7:
                        button7.setDisable(true);
                        break;
                    case 8:
                        button8.setDisable(true);
                        break;
                    case 9:
                        button9.setDisable(true);
                        break;
                    case 10:
                        button10.setDisable(true);
                        break;
                    case 11:
                        button11.setDisable(true);
                        break;
                    case 12:
                        button12.setDisable(true);
                        break;
                    case 13:
                        button13.setDisable(true);
                        break;
                    case 14:
                        button14.setDisable(true);
                        break;
                    case 15:
                        button15.setDisable(true);
                        break;
                    case 16:
                        button16.setDisable(true);
                        break;
                    case 17:
                        button17.setDisable(true);
                        break;
                    case 18:
                        button18.setDisable(true);
                        break;
                    case 19:
                        button19.setDisable(true);
                        break;
                    case 20:
                        button20.setDisable(true);
                        break;
                    case 21:
                        button21.setDisable(true);
                        break;
                    case 22:
                        button22.setDisable(true);
                        break;
                    case 23:
                        button23.setDisable(true);
                        break;
                    case 24:
                        button24.setDisable(true);
                        break;
                    case 25:
                        button25.setDisable(true);
                        break;
                    case 26:
                        button26.setDisable(true);
                        break;
                    case 27:
                        button27.setDisable(true);
                        break;
                    case 28:
                        button28.setDisable(true);
                        break;
                    case 29:
                        button29.setDisable(true);
                        break;
                    case 30:
                        button30.setDisable(true);
                        break;
                    case 31:
                        button31.setDisable(true);
                        break;
                    case 32:
                        button32.setDisable(true);
                        break;
                    case 33:
                        button33.setDisable(true);
                        break;
                    case 34:
                        button34.setDisable(true);
                        break;
                    case 35:
                        button35.setDisable(true);
                        break;
                    case 36:
                        button36.setDisable(true);
                        break;
                    case 37:
                        button37.setDisable(true);
                        break;
                    case 38:
                        button38.setDisable(true);
                        break;
                    case 39:
                        button39.setDisable(true);
                        break;
                    case 40:
                        button40.setDisable(true);
                        break;
                    case 41:
                        button41.setDisable(true);
                        break;
                    case 42:
                        button42.setDisable(true);
                        break;
                    case 43:
                        button43.setDisable(true);
                        break;
                    case 44:
                        button44.setDisable(true);
                        break;
                    case 45:
                        button45.setDisable(true);
                        break;
                    case 46:
                        button46.setDisable(true);
                        break;
                    case 47:
                        button47.setDisable(true);
                        break;
                    case 48:
                        button48.setDisable(true);
                        break;
                    case 49:
                        button49.setDisable(true);
                        break;
                    case 50:
                        button50.setDisable(true);
                        break;
                    case 51:
                        button51.setDisable(true);
                        break;
                    case 52:
                        button52.setDisable(true);
                        break;
                    case 53:
                        button53.setDisable(true);
                        break;
                    case 54:
                        button54.setDisable(true);
                        break;
                    default:
                        break;
                }
            }
        }
        toggleButtons.add(button1);
        toggleButtons.add(button2);
        toggleButtons.add(button3);
        toggleButtons.add(button4);
        toggleButtons.add(button5);
        toggleButtons.add(button6);
        toggleButtons.add(button7);
        toggleButtons.add(button8);
        toggleButtons.add(button9);
        toggleButtons.add(button10);
        toggleButtons.add(button11);
        toggleButtons.add(button12);
        toggleButtons.add(button13);
        toggleButtons.add(button14);
        toggleButtons.add(button15);
        toggleButtons.add(button16);
        toggleButtons.add(button17);
        toggleButtons.add(button18);
        toggleButtons.add(button19);
        toggleButtons.add(button20);
        toggleButtons.add(button21);
        toggleButtons.add(button22);
        toggleButtons.add(button23);
        toggleButtons.add(button24);
        toggleButtons.add(button25);
        toggleButtons.add(button26);
        toggleButtons.add(button27);
        toggleButtons.add(button28);
        toggleButtons.add(button29);
        toggleButtons.add(button30);
        toggleButtons.add(button31);
        toggleButtons.add(button32);
        toggleButtons.add(button33);
        toggleButtons.add(button34);
        toggleButtons.add(button35);
        toggleButtons.add(button36);
        toggleButtons.add(button37);
        toggleButtons.add(button38);
        toggleButtons.add(button39);
        toggleButtons.add(button40);
        toggleButtons.add(button41);
        toggleButtons.add(button42);
        toggleButtons.add(button43);
        toggleButtons.add(button44);
        toggleButtons.add(button45);
        toggleButtons.add(button46);
        toggleButtons.add(button47);
        toggleButtons.add(button48);
        toggleButtons.add(button49);
        toggleButtons.add(button50);
        toggleButtons.add(button51);
        toggleButtons.add(button52);
        toggleButtons.add(button53);
        toggleButtons.add(button54);
    }
}