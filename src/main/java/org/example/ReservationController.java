package org.example;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class ReservationController implements Initializable {
    @FXML
    private HBox myHBox;

    Stage stage = null;

    private List<Integer> checkedSeats = new ArrayList<Integer>();
    private Integer standardSeats = 0;
    private Integer vipSeats = 0;
    private Integer priceStandardSeats = 0;
    private Integer priceVipSeats = 0;
    private Integer priceAllSeats = 0;

    private Movie movie;

    @FXML
    private Text standardLabel, vipLabel, priceStandardLabel, priceVipLabel, priceAllLabel;

    @FXML
    private TextField imieNazwisko, email;

    @FXML
    private void switchToPrimary() throws IOException {
        standardSeats = 0;
        vipSeats = 0;
        priceStandardSeats = 0;
        priceVipSeats = 0;
        priceAllSeats = 0;
        SecondaryController.currentCheckedSeats.clear();
        App.setRoot("primary");
    }
    public void generatePdfFromHtml(String html) throws IOException, DocumentException {
        String outputFolder = System.getProperty("user.home") + File.separator + "kino_rezerwacja.pdf";
        OutputStream outputStream = new FileOutputStream(outputFolder);

        ITextRenderer renderer = new ITextRenderer();
        //renderer.getFontResolver().addFont(BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);

        outputStream.close();
    }

    private String parseThymeleafTemplate(String imie, String email, List<Integer> standardSeatsId, List<Integer> vipSeatsId, Integer priceAllSeats, String title, String day, String hour) {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding("UTF-8");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();
        context.setVariable("imieNazwisko", imie);
        context.setVariable("email", email);
        context.setVariable("standardSeatsId", standardSeatsId);
        context.setVariable("vipSeatsId", vipSeatsId);
        context.setVariable("priceAllSeats", priceAllSeats);
        context.setVariable("title", title);
        context.setVariable("day", day);
        context.setVariable("hour", hour);

        return templateEngine.process("ticketTemplate", context);
    }

    @FXML
    private void switchToReservationSuccess() throws IOException, DocumentException {
        String imie = imieNazwisko.getText();
        String em = email.getText();
        List<Integer> vipSeatsId = new ArrayList<>();
        List<Integer> standardSeatsId = new ArrayList<>();
        for(Integer cs: SecondaryController.currentCheckedSeats) {
            if (cs > 27 && cs < 37) {
                vipSeatsId.add(cs);
            } else {
                standardSeatsId.add(cs);
            }
        }
        movie = new Movie(SecondaryController.currentMovie);
        String title = movie.getTitle();
        String day = movie.getDay();
        String hour = SecondaryController.currentHour;


        String html = parseThymeleafTemplate(imie, em, standardSeatsId, vipSeatsId, priceAllSeats, title, day, hour);
        generatePdfFromHtml(html);
        SecondaryController.currentHall.setCheckedSeats(checkedSeats);
        standardSeats = 0;
        vipSeats = 0;
        priceStandardSeats = 0;
        priceVipSeats = 0;
        priceAllSeats = 0;
        SecondaryController.currentCheckedSeats.clear();
        App.setRoot("reservationSuccess");
    }

    @FXML
    private void exit() {
        standardSeats = 0;
        vipSeats = 0;
        priceStandardSeats = 0;
        priceVipSeats = 0;
        priceAllSeats = 0;
        SecondaryController.currentCheckedSeats.clear();
        Platform.exit();
    }

    @FXML
    private void minimize(ActionEvent event) throws IOException {
        stage = (Stage) myHBox.getScene().getWindow();
        stage.setIconified(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for(Integer s: SecondaryController.checkedSeats){
            this.checkedSeats.add(s);
        }
        for(Integer cs: SecondaryController.currentCheckedSeats){
            if(cs > 27 && cs < 37){
                vipSeats++;
            } else {
                standardSeats++;
            }
        }
        priceStandardSeats = 25 * standardSeats;
        priceVipSeats = 35 * vipSeats;
        priceAllSeats = priceStandardSeats + priceVipSeats;

        standardLabel.setText(String.valueOf(standardSeats));
        vipLabel.setText(String.valueOf(vipSeats));
        priceStandardLabel.setText(priceStandardSeats + " zł");
        priceVipLabel.setText(priceVipSeats + " zł");
        priceAllLabel.setText(priceAllSeats + " zł");
    }
}