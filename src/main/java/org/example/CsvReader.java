package org.example;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CsvReader {
	private static final String SEPARATOR= ",";
    List<Movie> records = new ArrayList<>();
//    private static String csvPath = "D:/Wszystko/PWSZ/II ROK/2semestr/cinema/src/main/resources/csv/movies.csv";
//    private String line = "";

    public CsvReader(){
    }
    public static List<Movie> getMoviesList() {
        try {
            String line;
            List<Movie> records = new ArrayList<>();
            //URL csvFileLocation = CsvReader.class.getResource("movies.csv");
            //File CSVFile = new File(csvFileLocation.getPath());
            String csvPath = "src/main/resources/csv/movies.csv";
            File CSVFile = new File(csvPath);
            BufferedReader br = new BufferedReader(new FileReader(CSVFile));
            while ((line = br.readLine()) != null) {
                String[] values = line.split(SEPARATOR);
                Movie movie = new Movie(values);
                records.add(movie);
            }
            return records;
        } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
