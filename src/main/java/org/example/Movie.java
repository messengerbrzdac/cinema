package org.example;

import java.util.ArrayList;

public class Movie {
	private String id;
	private String title;
	private String description_tag;
	//boolean isDubbing;
	private String dubbing_or_subtitles;
	private ArrayList<String> startingHours;
	private String day;

	public Movie(){

	}

	public Movie(Movie movie){
		this.id = movie.getId();
		this.title = movie.getTitle();
		this.description_tag = movie.getDescription_tag();
		//this.isDubbing = record[3].equals("DUBBING") ?  true : false;
		this.dubbing_or_subtitles = movie.getDubbing_or_subtitles();
		this.startingHours = movie.getStartingHours();
		this.day = movie.getDay();
	}

	public Movie(String[] record){
		this.id = record[0];
		this.title = record[1];
		this.description_tag = record[2];
		//this.isDubbing = record[3].equals("DUBBING") ?  true : false;
		this.dubbing_or_subtitles = record[3];
		String hours = record[4];
		String[] startingHours = hours.split(";");
		ArrayList<String> array = new ArrayList<>(3);
		for (String s : startingHours) {
			array.add(s);
		}
		this.startingHours = array;
        this.day = record[5];
	}

	public String getId() {
        return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription_tag() {
		return this.description_tag;
	}

	public String getDubbing_or_subtitles() {
		return this.dubbing_or_subtitles;
	}

	public ArrayList<String> getStartingHours() {
		return this.startingHours;
	}

	public String getDay() {
		return this.day;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription_tag(String description_tag) {
		this.description_tag = description_tag;
	}

	public void setDubbing_or_subtitles(String dubbing_or_subtitles) {
		this.dubbing_or_subtitles = dubbing_or_subtitles;
	}

	public void setStartingHours(ArrayList<String> startingHours) {
		this.startingHours = startingHours;
	}

	public void setDay(String day) {
		this.day = day;
	}

	@Override
	public String toString() {
		return "Tytul: " + this.title + "\nDzien tygodnia: " + this.day;
	}
}
