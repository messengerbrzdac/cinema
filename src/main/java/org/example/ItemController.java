package org.example;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class ItemController {

    @FXML
    private Label titleLabel;

    @FXML
    private Label tagLabel;

    @FXML
    private Label dosLabel;

    @FXML
    private Button hour1Label;

    @FXML
    private Button hour2Label;

    @FXML
    private Button hour3Label;

    @FXML
    private Label dayLabel;

    @FXML
    private void switchToSecondary1() throws IOException {
        SecondaryController.currentHallId = Integer.valueOf(movie.getId())*3-2;
        SecondaryController.currentMovie = new Movie(movie);
        SecondaryController.currentHour = hour1Label.getText();
        App.setRoot("secondary");
    }
    @FXML
    private void switchToSecondary2() throws IOException {
        SecondaryController.currentHallId = Integer.valueOf(movie.getId())*3-1;
        SecondaryController.currentMovie = new Movie(movie);
        SecondaryController.currentHour = hour2Label.getText();
        App.setRoot("secondary");
    }
    @FXML
    private void switchToSecondary3() throws IOException {
        SecondaryController.currentHallId = Integer.valueOf(movie.getId())*3;
        SecondaryController.currentMovie = new Movie(movie);
        SecondaryController.currentHour = hour3Label.getText();
        App.setRoot("secondary");
    }

    private Movie movie;

    private String currentHour;

    public void setData(Movie movie){
        this.movie = movie;
        titleLabel.setText(movie.getTitle());
        tagLabel.setText(movie.getDescription_tag());
        dosLabel.setText(movie.getDubbing_or_subtitles());
        ArrayList<String> hours = movie.getStartingHours();
        hour1Label.setText(hours.get(0));
        hour2Label.setText(hours.get(1));
        hour3Label.setText(hours.get(2));
        dayLabel.setText(movie.getDay());
    }
}
