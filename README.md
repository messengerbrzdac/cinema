
# Cinema

Application with beautiful interface which allows you to reserve for specific repertoire, 
show view of reserved and free seats. Application also have additional 
functions like reading repertoire from csv and reading it to the interface and 
printing ticket to pdf file. When choosing a place in the cinema room, 
we have the option of choosing standard seats as well as VIP seats.       

Application was created in Java version openjdk17

### How to run:               

Just run the project and follow the beautiful interface. \
After selecting the seats and entering your details your ticket will be saved in \
home directory named "kino_rezerwacja.pdf".

### Creators

>Michał Groński


Responsibilities:                                             
1.Class and functions with reading csv file\
2.Displaying movies form .csv file to repertoire\
3.Cinema halls class\
4.Hall system and seats system

>Patryk Gruszwski

Responsibilities:        
1.Creating interfaces and implementation to our Application\
2.Creating new scenes\
3.Ticket system and saving .pdf file to local drive.